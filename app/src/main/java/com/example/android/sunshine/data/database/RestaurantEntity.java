package com.example.android.sunshine.data.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "restaurants")
public class RestaurantEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String place;
    private String type;
    private String rating;
    private String reviews;
    private String icon;

    @Ignore
    public RestaurantEntity() {
    }

    @Ignore
    public RestaurantEntity(String name, String place, String type, String rating, String reviews, String icon) {
        this.name = name;
        this.place = place;
        this.type = type;
        this.rating = rating;
        this.reviews = reviews;
        this.icon = icon;
    }

    public RestaurantEntity(int id, String name, String place, String type, String rating, String reviews, String icon) {
        this.id = id;
        this.name = name;
        this.place = place;
        this.type = type;
        this.rating = rating;
        this.reviews = reviews;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    public String getType() {
        return type;
    }

    public String getRating() {
        return rating;
    }

    public String getReviews() {
        return reviews;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
