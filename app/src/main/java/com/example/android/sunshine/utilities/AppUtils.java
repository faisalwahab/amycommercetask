package com.example.android.sunshine.utilities;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.android.sunshine.BuildConfig;
import com.example.android.sunshine.R;

import java.util.Calendar;
import java.util.TimeZone;

public class AppUtils extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppContext.getInstance().setAppContext(getApplicationContext());


//        Logger.addLogAdapter(new AndroidLogAdapter() {
//            @Override
//            public boolean isLoggable(int priority, String tag) {
//                return BuildConfig.DEBUG;
//            }
//        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }

    public static void showLogs(String msg) {
//        Logger.d(msg);
        Log.d("AmyCommerce", msg);
    }

    public static void showToast(String msg) {
        if (!stringEmpty(msg))
            Toast.makeText(AppContext.getInstance().getAppContext(), msg, Toast.LENGTH_SHORT).show();
    }


    public static boolean stringEmpty(String str) {
        return !(str != null && !str.isEmpty() && str.length() > 0);
    }

    public static Fragment findFragment(FragmentActivity mActivity, String frag_tag) {
        return mActivity.getSupportFragmentManager().findFragmentByTag(frag_tag);
    }

    public static boolean isFragmentAvailable(FragmentActivity mActivity, String frag_tag) {
        return mActivity.getSupportFragmentManager().findFragmentByTag(frag_tag) != null;
    }

    public static void showFragment(FragmentActivity mActivity, Fragment fragment, int container, boolean onBackStack) {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.replace(container, fragment);
        if (onBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void showFragmentWithAnimation(FragmentActivity mActivity, Fragment fragment, int container, boolean onBackStack) {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.pull_in_right, R.anim.push_out_left, R.anim.pull_in_left, R.anim.push_out_right);
        transaction.replace(container, fragment);
        if (onBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void showFragmentWithCache(FragmentActivity mActivity, Fragment fragment, int container, boolean onBackStack) {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            transaction.show(fragment);
        } else {
            transaction.replace(container, fragment);
            if (onBackStack) {
                transaction.addToBackStack(null);
            }
        }
        transaction.commit();
    }

    public static void removeCurrentFragment(FragmentActivity mActivity, int container) {
        FragmentTransaction transaction = mActivity.getSupportFragmentManager().beginTransaction();
        Fragment currentFrag = mActivity.getSupportFragmentManager().findFragmentById(container);
        if (currentFrag != null)
            transaction.remove(currentFrag);
        transaction.commit();
        mActivity.getSupportFragmentManager().popBackStack();
    }

    public static Fragment findCurrentFragment(FragmentActivity mActivity, int container) {
        Fragment currentFrag = mActivity.getSupportFragmentManager().findFragmentById(container);
        return currentFrag;
    }

    public static void requestFocus(View view, FragmentActivity activity) {
        if (activity != null && view.requestFocus()) {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static String getAppVersionName() {
        return BuildConfig.VERSION_NAME;
    }

    public static String getAppVersionCode() {
        return String.valueOf(BuildConfig.VERSION_CODE);
    }


    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     * is network connected
     * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    /**
     * Get whether or not any network connection is present (eg. wifi, 3G, etc.).
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
        if (info == null) return false;
        for (int i = 0; i < info.length; i++)
            if (info[i].getState() == NetworkInfo.State.CONNECTED) return true;
        return false;
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) AppContext.getInstance().getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public static long getCurrentGmtTimeInMilli() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        return cal.getTimeInMillis();
    }

    public static String getAppNameByPackageId(String appPackageId) {
        PackageManager packageManager = AppContext.getInstance().getAppContext().getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(appPackageId, 0);
        } catch (final PackageManager.NameNotFoundException e) {
        }
        String title = (String) ((applicationInfo != null) ? packageManager.getApplicationLabel(applicationInfo) : "{unknown}");
        return title;
    }

}