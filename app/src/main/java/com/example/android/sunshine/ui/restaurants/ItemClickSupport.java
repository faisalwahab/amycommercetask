package com.example.android.sunshine.ui.restaurants;

import com.example.android.sunshine.data.database.ListRestaurantEntity;

public interface ItemClickSupport {
    void onItemClicked(ListRestaurantEntity position);
}
