package com.example.android.sunshine.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ModelRestaurant implements Parcelable {
    private String icon;
    private String id;
    private String name;
    private String place_id;
    private String rating;
    private List<String> types;
    private String vicinity;

    public ModelRestaurant() {
    }

    public ModelRestaurant(String icon, String id, String name, String place_id, String rating, List<String> types, String vicinity) {
        this.icon = icon;
        this.id = id;
        this.name = name;
        this.place_id = place_id;
        this.rating = rating;
        this.types = types;
        this.vicinity = vicinity;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.icon);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.place_id);
        dest.writeString(this.rating);
        dest.writeStringList(this.types);
        dest.writeString(this.vicinity);
    }

    protected ModelRestaurant(Parcel in) {
        this.icon = in.readString();
        this.id = in.readString();
        this.name = in.readString();
        this.place_id = in.readString();
        this.rating = in.readString();
        this.types = in.createStringArrayList();
        this.vicinity = in.readString();
    }

    public static final Parcelable.Creator<ModelRestaurant> CREATOR = new Parcelable.Creator<ModelRestaurant>() {
        @Override
        public ModelRestaurant createFromParcel(Parcel source) {
            return new ModelRestaurant(source);
        }

        @Override
        public ModelRestaurant[] newArray(int size) {
            return new ModelRestaurant[size];
        }
    };

    @Override
    public String toString() {
        return "ModelRestaurant{" +
                "icon='" + icon + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", place_id='" + place_id + '\'' +
                ", rating='" + rating + '\'' +
                ", types=" + types +
                ", vicinity='" + vicinity + '\'' +
                '}';
    }
}
