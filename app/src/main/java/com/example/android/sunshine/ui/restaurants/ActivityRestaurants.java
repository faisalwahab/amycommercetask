package com.example.android.sunshine.ui.restaurants;

import android.Manifest;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.android.sunshine.R;
import com.example.android.sunshine.data.database.ListRestaurantEntity;
import com.example.android.sunshine.utilities.AppContext;
import com.example.android.sunshine.utilities.InjectorUtils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class ActivityRestaurants extends AppCompatActivity implements ItemClickSupport, LifecycleOwner {

    private AdapterRestaurants mForecastAdapter;
    private RecyclerView mRecyclerView;
    private int mPosition = RecyclerView.NO_POSITION;
    private ProgressBar mLoadingIndicator;
    private RestaurantActivityViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);

        mRecyclerView = findViewById(R.id.rv_restaurants);
        mLoadingIndicator = findViewById(R.id.pb_loading_indicator);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setupMainPageRecyclerView();

        mForecastAdapter = new AdapterRestaurants(this);
        mRecyclerView.setAdapter(mForecastAdapter);

        RestaurantViewModelFactory factory = InjectorUtils.provideRestaurantActivityViewModelFactory(this.getApplicationContext());
        mViewModel = ViewModelProviders.of(this, factory).get(RestaurantActivityViewModel.class);


        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mViewModel.getRestaurants().observe(ActivityRestaurants.this, restaurantLists -> {
                            if (restaurantLists != null)
                                mForecastAdapter.setData(restaurantLists);

                            if (mPosition == RecyclerView.NO_POSITION) mPosition = 0;
                            mRecyclerView.smoothScrollToPosition(mPosition);

                            // Show the weather list or the loading screen based on whether the forecast data exists
                            // and is loaded
                            if (restaurantLists != null && restaurantLists.size() != 0)
                                showRestaurantDataView();
                            else showLoading();
                        });

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }

                })
                .check();
    }

    private void setupMainPageRecyclerView() {
        int firstVisibleItemPosition = 0;
        if (mRecyclerView != null && mRecyclerView.getLayoutManager() != null) {
            firstVisibleItemPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(AppContext.getInstance().getAppContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.scrollToPosition(firstVisibleItemPosition);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void showRestaurantDataView() {
        // First, hide the loading indicator
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        // Finally, make sure the weather data is visible
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        // Then, hide the weather data
        mRecyclerView.setVisibility(View.INVISIBLE);
        // Finally, show the loading indicator
        mLoadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClicked(ListRestaurantEntity position) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
