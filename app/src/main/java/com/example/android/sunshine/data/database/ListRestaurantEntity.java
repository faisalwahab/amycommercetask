package com.example.android.sunshine.data.database;

import android.os.Parcel;
import android.os.Parcelable;

public class ListRestaurantEntity implements Parcelable {
    private int id;
    private String name;
    private String place;
    private String type;
    private String rating;
    private String reviews;
    private String icon;

    public ListRestaurantEntity(int id, String name, String place, String type, String rating, String reviews, String icon) {
        this.id = id;
        this.name = name;
        this.place = place;
        this.type = type;
        this.rating = rating;
        this.reviews = reviews;
        this.icon = icon;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.place);
        dest.writeString(this.type);
        dest.writeString(this.rating);
        dest.writeString(this.reviews);
        dest.writeString(this.icon);
    }

    protected ListRestaurantEntity(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.place = in.readString();
        this.type = in.readString();
        this.rating = in.readString();
        this.reviews = in.readString();
        this.icon = in.readString();
    }

    public static final Creator<ListRestaurantEntity> CREATOR = new Creator<ListRestaurantEntity>() {
        @Override
        public ListRestaurantEntity createFromParcel(Parcel source) {
            return new ListRestaurantEntity(source);
        }

        @Override
        public ListRestaurantEntity[] newArray(int size) {
            return new ListRestaurantEntity[size];
        }
    };
}
