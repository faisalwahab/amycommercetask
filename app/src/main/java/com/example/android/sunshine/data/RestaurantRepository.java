/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.sunshine.data;

import android.arch.lifecycle.LiveData;
import android.util.Log;

import com.example.android.sunshine.AppExecutors;
import com.example.android.sunshine.data.database.ListRestaurantEntity;
import com.example.android.sunshine.data.database.RestaurantDao;
import com.example.android.sunshine.data.database.RestaurantEntity;
import com.example.android.sunshine.data.database.WeatherDao;
import com.example.android.sunshine.data.network.RestaurantNetworkDataSource;
import com.example.android.sunshine.data.network.WeatherNetworkDataSource;

import java.util.List;

/**
 * Handles data operations in Sunshine. Acts as a mediator between {@link RestaurantNetworkDataSource}
 * and {@link RestaurantDao}
 */
public class RestaurantRepository {
    private static final String LOG_TAG = RestaurantRepository.class.getSimpleName();

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static RestaurantRepository sInstance;
    private final RestaurantDao mWeatherDao;
    private final RestaurantNetworkDataSource mWeatherNetworkDataSource;
    private final AppExecutors mExecutors;
    private boolean mInitialized = false;

    private RestaurantRepository(RestaurantDao weatherDao,
                                 RestaurantNetworkDataSource weatherNetworkDataSource,
                                 AppExecutors executors) {
        mWeatherDao = weatherDao;
        mWeatherNetworkDataSource = weatherNetworkDataSource;
        mExecutors = executors;

        // As long as the repository exists, observe the network LiveData.
        // If that LiveData changes, update the database.
        LiveData<RestaurantEntity[]> networkData = mWeatherNetworkDataSource.getAllRestaurants();
        networkData.observeForever(newForecastsFromNetwork -> {
            mExecutors.diskIO().execute(() -> {

                Log.d(LOG_TAG, "Old weather deleted");
                // Insert our new weather data into Sunshine's database
                mWeatherDao.bulkInsert(newForecastsFromNetwork);
                Log.d(LOG_TAG, "New values inserted");
            });
        });
    }

    public synchronized static RestaurantRepository getInstance(
            RestaurantDao weatherDao, RestaurantNetworkDataSource weatherNetworkDataSource,
            AppExecutors executors) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new RestaurantRepository(weatherDao, weatherNetworkDataSource,
                        executors);
                Log.d(LOG_TAG, "Made new repository");
            }
        }
        return sInstance;
    }

    /**
     * Creates periodic sync tasks and checks to see if an immediate sync is required. If an
     * immediate sync is required, this method will take care of making sure that sync occurs.
     */
    private synchronized void initializeData() {

        // Only perform initialization once per app lifetime. If initialization has already been
        // performed, we have nothing to do in this method.
        if (mInitialized) return;
        mInitialized = true;

        // This method call triggers Sunshine to create its task to synchronize weather data
        // periodically.
//        mWeatherNetworkDataSource.scheduleRecurringFetchRestaurantSync();

        mExecutors.diskIO().execute(() -> {
//            if (isFetchNeeded()) {
                startFetchRestaurantService();
//            }
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<List<ListRestaurantEntity>> getAllRestaurant() {
        initializeData();
        return mWeatherDao.getCurrentAllRestaurant();
    }

    /**
     * Checks if there are enough days of future weather for the app to display all the needed data.
     *
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded() {
        int count = mWeatherDao.countAllRestaurant();
        return (count < WeatherNetworkDataSource.NUM_DAYS);
    }

    /**
     * Network related operation
     */

    private void startFetchRestaurantService() {
        mWeatherNetworkDataSource.startFetchRestaurantService();
    }

}