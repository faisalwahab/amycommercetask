package com.example.android.sunshine.data.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class RestaurantResponse implements Parcelable {

    private List<ModelRestaurant> results;
    private String status;

    public RestaurantResponse() {
    }

    public RestaurantResponse(List<ModelRestaurant> results, String status) {
        this.results = results;
        this.status = status;
    }

    public List<ModelRestaurant> getResults() {
        return results;
    }

    public void setResults(List<ModelRestaurant> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.results);
        dest.writeString(this.status);
    }

    protected RestaurantResponse(Parcel in) {
        this.results = in.createTypedArrayList(ModelRestaurant.CREATOR);
        this.status = in.readString();
    }

    public static final Parcelable.Creator<RestaurantResponse> CREATOR = new Parcelable.Creator<RestaurantResponse>() {
        @Override
        public RestaurantResponse createFromParcel(Parcel source) {
            return new RestaurantResponse(source);
        }

        @Override
        public RestaurantResponse[] newArray(int size) {
            return new RestaurantResponse[size];
        }
    };

    @Override
    public String toString() {
        return "RestaurantResponse{" +
                "results=" + results +
                ", status='" + status + '\'' +
                '}';
    }
}
