package com.example.android.sunshine.ui.restaurants;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.sunshine.R;
import com.example.android.sunshine.data.database.ListRestaurantEntity;
import com.example.android.sunshine.utilities.AppContext;
import com.example.android.sunshine.utilities.ImageViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterRestaurants extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ListRestaurantEntity> listRestaurant = new ArrayList<>();
    private int mRowIndex = -1;

    private ItemClickSupport mOnItemClickListener;

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        // replacement of findViewById, implement butter-knife lib
        @BindView(R.id.iv_restaurant_photo)
        ImageView iv_restaurant_photo;
        @BindView(R.id.tv_restaurant_name)
        TextView tv_restaurant_name;
        @BindView(R.id.tv_restaurant_location)
        TextView tv_restaurant_location;
        @BindView(R.id.tv_restaurant_type)
        TextView tv_restaurant_type;
        @BindView(R.id.tv_restaurant_rating)
        TextView tv_restaurant_rating;
        @BindView(R.id.tv_restaurant_reviews)
        TextView tv_restaurant_reviews;

        ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public AdapterRestaurants(ItemClickSupport mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public void setData(List<ListRestaurantEntity> data) {
        if (listRestaurant != data) {
            listRestaurant = data;
            notifyDataSetChanged();
        }
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(AppContext.getInstance().getAppContext()).inflate(R.layout.adaptor_restaurant_item, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder rawHolder, int position) {
        ItemViewHolder holder = (ItemViewHolder) rawHolder;


        ListRestaurantEntity place = listRestaurant.get(position);
        if (place != null) {
            holder.tv_restaurant_name.setText(place.getName());
            holder.tv_restaurant_location.setText(place.getPlace());
            holder.tv_restaurant_rating.setText(place.getRating());

            ImageViewUtils.getInstance().loadImages(holder.iv_restaurant_photo, place.getIcon(), R.drawable.restaurant_place_holder);
        }

        // Detect the click events and pass them to any listeners
        holder.itemView.setOnClickListener(v -> {
            if (null != mOnItemClickListener) {
                mOnItemClickListener.onItemClicked(listRestaurant.get(position));
            }
        });
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return listRestaurant.size();
    }
}
