package com.example.android.sunshine.utilities;

import android.widget.ImageView;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;


public class ImageViewUtils {
    private static ImageViewUtils INSTANCE = null;

    public static ImageViewUtils getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ImageViewUtils();
        return INSTANCE;
    }

    private ImageViewUtils() {
    }

    public void loadImages(final ImageView iv, final String imgUrl, int place_holder) {
        if (iv == null)
            return;

        Glide.with(iv.getContext())
                .asBitmap() // All our images are static, we want to display them as bitmaps
                .load(imgUrl)
                .transition(GenericTransitionOptions.with(android.R.anim.fade_in)) // need to manually set the animation as bitmap cannot use cross fade
                .apply(new RequestOptions()
                        .placeholder(place_holder)
                        .centerCrop() // scale type
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .format(DecodeFormat.PREFER_RGB_565) // the decode format - this will not use alpha at all
                )
                .thumbnail(0.2f) // make use of the thumbnail which can display a down-sized version of the image
                .into(iv);
    }

}
