package com.example.android.sunshine.utilities;

import android.content.Context;

public class AppContext {
    private static AppContext INSTANCE = null;

    private Context mContext = null;

    public static AppContext getInstance() {
        if (INSTANCE == null)
            INSTANCE = new AppContext();
        return INSTANCE;
    }

    private AppContext() {
    }

     void setAppContext(Context context) {
        this.mContext = context;
    }

    public Context getAppContext() {
        if (this.mContext == null)
            throw new NullPointerException("Context should not be null");
        return this.mContext;
    }

}
